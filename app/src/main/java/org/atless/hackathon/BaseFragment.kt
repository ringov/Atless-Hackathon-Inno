package org.atless.hackathon

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

abstract class BaseFragment : Fragment() {

    abstract fun getLayout(): Int

    @CallSuper
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? = container?.inflate(getLayout())

    fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, message, length).show()
    }

    fun toast(@StringRes stringRes: Int, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(context, stringRes, length).show()
    }
}