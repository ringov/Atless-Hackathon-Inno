package org.atless.hackathon

import android.content.res.Resources
import android.graphics.Color
import android.support.graphics.drawable.VectorDrawableCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.util.*

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false) = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)

fun View.setVisible(visible: Boolean, keepSpace: Boolean = false) = if (visible) show() else hide(keepSpace)

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide(keepSpace: Boolean = false) {
    visibility = if (keepSpace) View.INVISIBLE else View.GONE
}

fun Resources.dp(dp: Int) = ((dp * displayMetrics.density) + 0.5f).toInt()

fun View.dp(dp: Int) = resources.dp(dp)

fun Resources.getVectorDrawable(drawableId: Int, color: String) = VectorDrawableCompat.create(this, drawableId, null).let {
    it?.let { DrawableCompat.setTint(it.mutate(), Color.parseColor(color)) }
    it
}

fun <E> List<E>.random(): E? = if (size > 0) get(Random().nextInt(size)) else null
