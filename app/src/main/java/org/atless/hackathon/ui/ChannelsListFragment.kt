package org.atless.hackathon.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_channels_list.*
import org.atless.hackathon.R
import org.atless.hackathon.adapters.BaseRecyclerAdapter
import org.atless.hackathon.adapters.ChannelDelegate
import org.atless.hackathon.adapters.OffsetDecoration
import org.atless.hackathon.adapters.OffsetResolver
import org.atless.hackathon.data.Model
import org.atless.hackathon.inflate

class ChannelsListFragment : Fragment() {

    lateinit var adapter: BaseRecyclerAdapter

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_channels_list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = BaseRecyclerAdapter(ChannelDelegate(Model::switchChannelSubscription))
        rvChannels.setHasFixedSize(true)
        rvChannels.layoutManager = LinearLayoutManager(context)
        rvChannels.adapter = adapter
        rvChannels.addItemDecoration(OffsetDecoration(OffsetResolver.ChannelsListResolver()))
    }

    override fun onResume() {
        super.onResume()
        disposable = Model.subscribedChannelsObservable.subscribe({ adapter.items = it })
    }

    override fun onPause() {
        disposable?.dispose()
        disposable = null
        super.onPause()
    }
}
