package org.atless.hackathon.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import kotlinx.android.synthetic.main.fragment_info.*
import org.atless.hackathon.BaseFragment
import org.atless.hackathon.Log
import org.atless.hackathon.R
import org.atless.hackathon.data.Model
import org.atless.hackathon.data.SecretChannels
import android.view.inputmethod.InputMethodManager
import org.atless.hackathon.Metrica

class InfoFragment : BaseFragment() {

    override fun getLayout() = R.layout.fragment_info

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        etChannel.setOnEditorActionListener { _, _, _ ->
            val result = tryAddChannel(etChannel.text.toString().toLowerCase())
            if (!result) {
                tilChannel.error = "Неверно, проявите смекалку!"
            }

            true
        }
        etChannel.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                tilChannel.error = ""
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        etChannel.setOnFocusChangeListener { _, hasFocus ->
            app_bar.setExpanded(!hasFocus)
            if (!hasFocus) {
                val imm = (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
        tvRingov.setOnClickListener { openTelegramChat("ringov") }
        tvIvanAntsiferov.setOnClickListener { openTelegramChat("ivanantsiferov") }
    }

    private fun openTelegramChat(user: String) {
        val intent = if (MainActivity.INSTANCE?.isAppInstalled("org.telegram.messenger") == true) {
            Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=$user"))
        } else {
            Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/$user"))
        }
        activity?.startActivity(intent)
    }

    private fun tryAddChannel(text: String): Boolean {
        val channel = when {
            text == "саентология" -> SecretChannels.saentology
            else -> null
        }

        if (channel != null) {
            Model.channels.add(channel)
            channel.events.forEach { Model.events.add(it) }
            Model.emitChannels()
            toast("Канал ${channel.title} разблокирован!")
            etChannel.clearFocus()
        }

        Metrica.sendSecretChannelAddTry(text)
        Log.d("try add secret channel; $text")
        return channel != null
    }
}
