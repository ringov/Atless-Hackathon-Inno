package org.atless.hackathon.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_event_details.*
import org.atless.hackathon.R
import org.atless.hackathon.adapters.*
import org.atless.hackathon.data.Event
import org.atless.hackathon.data.Model
import org.atless.hackathon.inflate

class FragmentEventDetails : Fragment() {

    lateinit var adapter: BaseRecyclerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_event_details)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        adapter = BaseRecyclerAdapter(DonateBlockDelegate(), DescriptionBlockDelegate(), DateTimeBlockDelegate())
        adapter.setHasStableIds(true)
        rvEventBlocks.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context)
        rvEventBlocks.layoutManager = LinearLayoutManager(context)
        rvEventBlocks.adapter = adapter
        rvEventBlocks.addItemDecoration(DividerItemDecoration(rvEventBlocks.context, layoutManager.orientation))
        rvEventBlocks.addItemDecoration(OffsetDecoration(OffsetResolver.EventBlockResolver()))
        ibBack.setOnClickListener { activity?.onBackPressed() }

        Model.event?.let(this::showEventInfo)

        Model.observeEventClicked()
                .subscribe(this::showEventInfo)
    }

    private fun showEventInfo(event: Event) {
        tvTitle.text = event.title
        tvDescription.text = event.description
        val channel = Model.getChannel(event.channelId)
        fabIcon.setImageResource(channel.iconRes)
        adapter.items = event.blocks
        tvChannel.text = Model.channels.first { it.id == event.channelId }.title
    }
}
