package org.atless.hackathon.ui

import android.Manifest
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_sliding.*
import org.atless.hackathon.*
import org.atless.hackathon.data.Model
import org.atless.hackathon.machinelearning.YaSpeech
import pub.devrel.easypermissions.EasyPermissions
import android.content.pm.PackageManager
import android.content.pm.PackageInfo



class MainActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks {

    companion object {
        private const val RC_RECORD_AUDIO = 823

        var INSTANCE: MainActivity? = null
    }

    internal var prevMenuItem: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViewPager(viewpager)

        navigation.selectedItemId = R.id.navMap
        viewpager.currentItem = 1
        viewpager.offscreenPageLimit = 3

        navigation.setOnNavigationItemSelectedListener(
                { item ->
                    if (prevMenuItem != null) {
                        if (prevMenuItem !== item) {
                            // hide hint if another menu is chosen
                        }
                    }

                    //if (slideLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
                    //}
                    when (item.itemId) {
                        R.id.navChannels -> {
                            viewpager.currentItem = 0
                            fabAdd.show()
                        }
                        R.id.navMap -> {
                            viewpager.currentItem = 1
                            fabAdd.show()
                        }
                        R.id.navAbout -> {
                            viewpager.currentItem = 2
                            fabAdd.hide()
                        }
                    }
                    false
                })

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.shitContainer, FragmentEventDetails())
                    .commit()
        }

        updateFabIcon()
        fabMic.setOnClickListener { fabClick() }
        fabAdd.setOnClickListener {
            Metrica.sendAddFabClick()
            toast(App.getRandomRofl())
        }
        requestAudioRecording()

        INSTANCE = this

        Model.observeEventClicked()
                .subscribe({ event ->
                    sliding_layout.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                })

        progressBar.setOnClickListener {
            YaSpeech.stopTolkin()
            updateFabIcon()
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    private fun requestAudioRecording() {
        val perms = arrayOf(Manifest.permission.RECORD_AUDIO)
        if (EasyPermissions.hasPermissions(this, *perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, "Разрешите доступ к микрофону, чтобы использовать голосового помощника.", RC_RECORD_AUDIO, *perms)
        }
    }

    fun updateFabIcon() {
        if (YaSpeech.isListening) {
            fabMic.setImageResource(R.drawable.ic_stop_black_24dp)
        } else {
            fabMic.setImageResource(R.drawable.ic_mic_black_24dp)
        }
        speechFinished()
    }

    private fun fabClick() {
        YaSpeech.isListening = !YaSpeech.isListening
        if (YaSpeech.isListening) {
            YaSpeech.recognize()
        } else {
            YaSpeech.stopListening()
        }
        updateFabIcon()
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        fabMic.hide()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        // awesome
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                prevMenuItem?.isChecked = false

                navigation.menu.getItem(position).isChecked = true
                prevMenuItem = navigation.menu.getItem(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (sliding_layout.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        } else {
            super.onBackPressed()
        }
    }

    fun speechInProgress() {
        fabMic.setVisible(false)
        progressBar.setVisible(true)
    }

    fun speechFinished() {
        progressBar.setVisible(false)
        fabMic.setVisible(true)
    }

    fun isAppInstalled(package_name: String): Boolean {
        try {
            val pm = packageManager
            val info = pm.getPackageInfo(package_name, PackageManager.GET_META_DATA)
            return true
        } catch (e: PackageManager.NameNotFoundException) {
            return false
        }

    }
}
