package org.atless.hackathon.data

import org.atless.hackathon.adapters.AdapterItem

data class Event(
        val id: Long,
        val title: String,
        val description: String,
        val lat: Double,
        val lon: Double,
        val channelId: Long,
        val blocks: List<EventBlock>
) : AdapterItem
