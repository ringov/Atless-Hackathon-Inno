package org.atless.hackathon.data

import org.atless.hackathon.R

object SecretChannels {

    val saentology = Channel(
            1000,
            "Саентология",
            "Поле битвы - земля",
            R.drawable.ic_saentology,
            arrayListOf(
                    Event(
                            1000,
                            "Застрял в чулане",
                            "Застрял в шкафу вместе с другом, не могу выбраться!",
                            55.751945,
                            48.748323,
                            1000,
                            emptyList()
                    )
            ),
            "",
            true
    )
}
