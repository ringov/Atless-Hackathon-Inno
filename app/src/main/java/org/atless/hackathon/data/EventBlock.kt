package org.atless.hackathon.data

import org.atless.hackathon.adapters.AdapterItem

sealed class EventBlock : AdapterItem {

    class DonateBlock : EventBlock()

    class DescriptionBlock(val text: String) : EventBlock()

    class DateTimeBlock(val start: String, val end: String) : EventBlock()
}
