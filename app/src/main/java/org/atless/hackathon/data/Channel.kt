package org.atless.hackathon.data

import android.support.annotation.DrawableRes
import org.atless.hackathon.adapters.AdapterItem

data class Channel(
        val id: Long,
        val title: String,
        val description: String,
        @DrawableRes val iconRes: Int,
        val events: ArrayList<Event>,
        val color: String,
        var isSubscribed: Boolean
) : AdapterItem
