package org.atless.hackathon.adapters

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import kotlinx.android.synthetic.main.item_channel.view.*
import org.atless.hackathon.*
import org.atless.hackathon.data.Channel

class ChannelDelegate(
        private val onSubscriptionSwitch: (Channel) -> Unit
) : AbsListItemAdapterDelegate<Channel, AdapterItem, ChannelDelegate.ChannelViewHolder>() {

    override fun isForViewType(item: AdapterItem, items: MutableList<AdapterItem>, position: Int): Boolean {
        return item is Channel
    }

    override fun onCreateViewHolder(parent: ViewGroup): ChannelViewHolder {
        return ChannelViewHolder(parent.inflate(R.layout.item_channel))
    }

    override fun onBindViewHolder(item: Channel, viewHolder: ChannelViewHolder, payloads: MutableList<Any>) = with(viewHolder.itemView) {
        tvTitle.text = item.title
        tvDescription.text = item.description
        val hideDesc = item.description.isEmpty()
        tvDescription.setVisible(!hideDesc)
        tvTitle.layoutParams.height = dp(if (hideDesc) 56 else 28)
        tvTitle.gravity = if (hideDesc) Gravity.CENTER_VERTICAL else Gravity.BOTTOM
        ivIcon.setImageResource(item.iconRes)

        swSubscription.isChecked = item.isSubscribed
        swSubscription.setOnClickListener { onSubscriptionSwitch(item) }
    }

    class ChannelViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
