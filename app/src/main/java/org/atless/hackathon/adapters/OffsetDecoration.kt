package org.atless.hackathon.adapters

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class OffsetDecoration(private vararg val resolvers: OffsetResolver) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val pos = parent.getChildAdapterPosition(view)
        val item = (parent.adapter as BaseRecyclerAdapter).getItem(pos)

        for (resolver in resolvers) {
            resolver.calculate(parent.resources, outRect, parent.adapter as BaseRecyclerAdapter, pos, item)
        }
    }
}
