package org.atless.hackathon.adapters

import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import org.atless.hackathon.R
import org.atless.hackathon.data.EventBlock
import org.atless.hackathon.inflate

class DonateBlockDelegate : AbsListItemAdapterDelegate<EventBlock.DonateBlock, AdapterItem, ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_block_donate))
    }

    override fun isForViewType(item: AdapterItem, items: MutableList<AdapterItem>, position: Int): Boolean {
        return item is EventBlock.DonateBlock
    }

    override fun onBindViewHolder(item: EventBlock.DonateBlock, viewHolder: ViewHolder, payloads: MutableList<Any>) {
    }
}
