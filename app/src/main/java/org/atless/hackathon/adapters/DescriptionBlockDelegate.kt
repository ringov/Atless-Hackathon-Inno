package org.atless.hackathon.adapters

import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import kotlinx.android.synthetic.main.item_block_description.view.*
import org.atless.hackathon.R
import org.atless.hackathon.data.EventBlock
import org.atless.hackathon.inflate

class DescriptionBlockDelegate : AbsListItemAdapterDelegate<EventBlock.DescriptionBlock, AdapterItem, ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_block_description))
    }

    override fun isForViewType(item: AdapterItem, items: MutableList<AdapterItem>, position: Int): Boolean {
        return item is EventBlock.DescriptionBlock
    }

    override fun onBindViewHolder(item: EventBlock.DescriptionBlock, viewHolder: ViewHolder, payloads: MutableList<Any>) = with(viewHolder) {
        itemView.tvDescription.text = item.text
    }
}
