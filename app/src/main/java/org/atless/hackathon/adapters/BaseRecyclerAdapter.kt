package org.atless.hackathon.adapters

import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter

typealias Items = List<AdapterItem>

class BaseRecyclerAdapter(
        vararg delegates: AdapterDelegate<Items>,
        items: Items = emptyList()
) : ListDelegationAdapter<Items>() {

    init {
        this.items = items
        for (delegate in delegates) {
            delegatesManager.addDelegate(delegate)
        }
    }

    fun getItem(index: Int) = items.getOrNull(index)

    override fun setItems(items: Items) {
        super.setItems(items)
        notifyDataSetChanged()
    }
}
