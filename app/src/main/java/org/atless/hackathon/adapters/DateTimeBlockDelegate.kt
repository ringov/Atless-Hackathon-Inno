package org.atless.hackathon.adapters

import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AbsListItemAdapterDelegate
import kotlinx.android.synthetic.main.item_block_date_time.view.*
import org.atless.hackathon.R
import org.atless.hackathon.data.EventBlock
import org.atless.hackathon.inflate

class DateTimeBlockDelegate : AbsListItemAdapterDelegate<EventBlock.DateTimeBlock, AdapterItem, ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_block_date_time))
    }

    override fun isForViewType(item: AdapterItem, items: MutableList<AdapterItem>, position: Int): Boolean {
        return item is EventBlock.DateTimeBlock
    }

    override fun onBindViewHolder(item: EventBlock.DateTimeBlock, viewHolder: ViewHolder, payloads: MutableList<Any>) = with(viewHolder.itemView) {
        tvStart.text = item.start
        tvEnd.text = item.end
    }
}
