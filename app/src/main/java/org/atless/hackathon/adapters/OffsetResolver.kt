package org.atless.hackathon.adapters

import android.content.res.Resources
import android.graphics.Rect
import org.atless.hackathon.data.Channel
import org.atless.hackathon.data.EventBlock
import org.atless.hackathon.dp
import kotlin.math.max

sealed class OffsetResolver {

    fun top(rect: Rect, offset: Int) = with(rect) { top = max(rect.top, offset) }

    fun bottom(rect: Rect, offset: Int) = with(rect) { bottom = max(rect.bottom, offset) }

    fun right(rect: Rect, offset: Int) = with(rect) { right = max(rect.right, offset) }

    fun left(rect: Rect, offset: Int) = with(rect) { left = max(rect.left, offset) }

    abstract fun calculate(res: Resources, rect: Rect, adapter: BaseRecyclerAdapter, pos: Int, item: Any?)

    class EventBlockResolver : OffsetResolver() {
        override fun calculate(res: Resources, rect: Rect, adapter: BaseRecyclerAdapter, pos: Int, item: Any?) {
            if (item is EventBlock && pos == 0) {
                top(rect, res.dp(32))
            }
            if (item is EventBlock && pos == adapter.items.lastIndex) {
                bottom(rect, res.dp(156))
            }
        }
    }

    class ChannelsListResolver : OffsetResolver() {
        override fun calculate(res: Resources, rect: Rect, adapter: BaseRecyclerAdapter, pos: Int, item: Any?) {
            when {
                item is Channel && pos == adapter.items.lastIndex -> {
                    bottom(rect, res.dp(128))
                }
            }
        }
    }
}
