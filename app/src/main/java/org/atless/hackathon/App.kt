package org.atless.hackathon

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.mapbox.mapboxsdk.Mapbox
import com.yandex.metrica.YandexMetrica
import java.util.*

class App : Application() {

    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context

        fun getRandomRofl(): String {
            return listOf(
                    "В процессе доработки, шеф.",
                    "Спасибо за проявленный к этой кнопке интерес!",
                    "Это хакатон! Вы ведь не думали, что все будет работать?",
                    "Не сделали еще, сорян."
            ).random().orEmpty()
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        Mapbox.getInstance(this,
                "pk.eyJ1Ijoic3Bpcml0aXplZCIsImEiOiJjajA1YXptYXcwMGVhMzJwbnpseXNvbXBxIn0.T0gVSeSA4qW2wWN_Qg_tMg")

        YandexMetrica.activate(applicationContext, "64c123f3-ad64-4d3b-b2f0-363add454746")
        // Отслеживание активности пользователей
        YandexMetrica.enableActivityAutoTracking(this)
    }
}
