package org.atless.hackathon.map

import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.SupportMapFragment
import org.atless.hackathon.Metrica
import org.atless.hackathon.data.Event
import org.atless.hackathon.data.Model

class MapFragment : SupportMapFragment(), OnMapReadyCallback {

    private var map: MapboxMap? = null

    private val eventsMap: HashMap<String, Long> = HashMap()
    private var events: HashMap<Long, Event> = HashMap()

    companion object {
        private const val MIN_ZOOM = 13.7
        private const val MAX_ZOOM = 17.0
        private const val DEFAULT_ZOOM = 14.3
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getMapAsync(this)
    }

    override fun onMapReady(map: MapboxMap) {
        this.map = map
        initStyle(map)
        initBoundaries(map)
        initCameraAngle(map)
        initCurrentPosition(map)
        initClickListener(map)

        Model.observeAllUnmutedEvents()
                .subscribe(this::showEvents)
    }

    private fun initClickListener(map: MapboxMap) {
        map.setOnMarkerClickListener {
            val event = eventsMap[it.snippet]!!
            Model.openEvent(event)
            Metrica.sendMapPinTap(event)
            true
        }
    }

    private fun showEvents(events: List<Event>) {
        events.forEach {
            this.events[it.id] = it
            this.eventsMap[it.id.toString()] = it.id
        }
        if (map != null) {
            map?.clear()
            val ctx = context!!
            val iconFactory = IconFactory.getInstance(ctx)

            for (event in events) {
                val channel = Model.getChannel(event.channelId)
                val icon = iconFactory.fromBitmap(getBitmapFromVectorDrawable(channel.iconRes))
                map?.addMarker(MarkerOptions()
                        .position(LatLng(event.lat, event.lon))
                        .icon(icon)
                        .snippet(event.id.toString())
                        .title(event.title))
            }
        }
    }

    private fun getBitmapFromVectorDrawable(drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context!!, drawableId)!!
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun initStyle(map: MapboxMap) {
        map.setStyleUrl("asset://map_style.json")
    }

    private fun initCurrentPosition(map: MapboxMap) {
        map.moveCamera(CameraUpdateFactory
                .newLatLngZoom(LatLng(55.751257, 48.746540), DEFAULT_ZOOM))
    }

    private fun initCameraAngle(map: MapboxMap) {
        map.cameraPosition = CameraPosition.Builder()
                .tilt(30.0)
                .build()
    }

    private fun initBoundaries(map: MapboxMap) {
        map.setMaxZoomPreference(MAX_ZOOM)
        map.setMinZoomPreference(MIN_ZOOM)
        val bounds = LatLngBounds.Builder()
                .include(LatLng(55.746896, 48.724329))
                .include(LatLng(55.733759, 48.740551))
                .include(LatLng(55.753005, 48.765271))
                .include(LatLng(55.759031, 48.739478))
                .build()
        map.setLatLngBoundsForCameraTarget(bounds)
    }
}