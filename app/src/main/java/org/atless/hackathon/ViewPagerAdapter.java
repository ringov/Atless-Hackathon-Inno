package org.atless.hackathon;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.atless.hackathon.ui.InfoFragment;
import org.atless.hackathon.map.MapFragment;
import org.atless.hackathon.ui.ChannelsListFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ChannelsListFragment();
            case 1:
                return new MapFragment();
            case 2:
                return new InfoFragment();
            default:
                return new MapFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
