package org.atless.hackathon.machinelearning

import org.atless.hackathon.App
import org.atless.hackathon.Log
import org.atless.hackathon.R
import org.atless.hackathon.data.Channel
import org.atless.hackathon.data.Model
import org.atless.hackathon.random

object SpeechProcessor {

    fun analyze(text: String) {
        val answer = when {
            text.recognize(Action.Subscribe, Entity.Channel) -> buildSubscribedChannelsAnswer()
            checkEventsListTrigger(text) -> buildNearEventsReviewAnswer()
            checkRandomEventTrigger(text) -> buildRandomEventAnswer()
            else -> "Я не поняла."
        }
        Log.d("SpeechProcessor.analyze().answer: $answer")
        YaSpeech.vocalize(answer)
    }

    private fun String.recognize(vararg words: Word): Boolean {
        return words.all { this.hasWords(*it.vocab) }
    }

    private fun String.hasWords(vararg words: CharSequence): Boolean {
        return words.any { this.contains(it, true) }
    }

    private fun checkRandomEventTrigger(text: String): Boolean = when {
        text.recognize(Other.Recommend) -> true
        else -> false
    }

    private fun checkEventsListTrigger(text: String): Boolean = when {
        text.recognize(Other.Interesting, Entity.Event) -> true
        text.recognize(Other.Nearby) -> true
        else -> false
    }

    private fun buildSubscribedChannelsAnswer(): String {
        val count = Model.channels.filter { it.isSubscribed }.count()
        return App.context.resources.getQuantityString(R.plurals.channelsCountPlurals, count, count)
    }

    private fun buildNearEventsReviewAnswer(): String {
        val events = Model.channels.filter(Channel::isSubscribed).filter { it.id != 0L }.flatMap(Channel::events)
        var answer = "Рядом с вами ${App.context.resources.getQuantityString(R.plurals.eventsCountPlurals, events.count(), events.count())}"
        if (events.isNotEmpty()) {
            answer += ", сейчас всё расскажу!"
            for (event in events) {
                answer += "${event.title}. ${event.description}. "
            }
            answer += " Хорошего дня!"
        } else {
            answer += ", похоже вы в степи."
        }
        return answer
    }

    private fun buildRandomEventAnswer(): String {
        val event = Model.events.toSet().filter { it.id != 0L }.random()
        when {
            event != null -> {
                val channel = Model.getChannel(event.channelId)
                val beginOfMessage = listOf(
                        "Обратите внимание на",
                        "Возможно, вам будет интересно",
                        "Могу посоветовать"
                ).random().orEmpty()
                return "$beginOfMessage ${event.title} в канале ${channel.title}."
            }
            else -> return  "Мне нечего вам посоветовать."
        }
    }
}
