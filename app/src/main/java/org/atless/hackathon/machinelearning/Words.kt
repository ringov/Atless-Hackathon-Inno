package org.atless.hackathon.machinelearning

enum class Action(override val vocab: Array<String>) : Word {
    Subscribe(arrayOf("подписан", "подписана", "слежу", "отслеживаю", "наблюдаю", "подписках", "подписался", "подписалась", "подписок")),
    Happening(arrayOf("происходит", "творится"))
}

enum class Other(override val vocab: Array<String>) : Word {
    Interesting(arrayOf("интересного", "интересное", "интересный", "интересные")),
    Nearby(arrayOf("вокруг", "рядом", "около", "недалеко", "близко", "поблизости")),
    Recommend(arrayOf("посоветуешь", "че интересного", "чего интересного", "что интересного", "посоветуй", "куда сходить", "заняться", "порекомендуй", "что нового", "чего нового", "че нового"))
}

enum class Entity(override val vocab: Array<String>) : Word {
    Channel(arrayOf("канал", "каналы", "каналов", "каналами", "канала", "каналу", "каналом", "канале", "каналам", "каналах")),
    Event(arrayOf("событие", "событий", "событием", "события", "событию", "событии", "событиям", "событиями", "событиях")),
}
