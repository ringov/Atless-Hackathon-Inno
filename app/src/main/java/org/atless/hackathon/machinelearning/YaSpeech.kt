package org.atless.hackathon.machinelearning

import android.annotation.SuppressLint
import org.atless.hackathon.App
import org.atless.hackathon.Log
import org.atless.hackathon.ui.MainActivity
import ru.yandex.speechkit.*

object YaSpeech : VocalizerListener, RecognizerListener {

    var isListening = false

    private var recognizer: Recognizer? = null
    private var vocalizer: Vocalizer? = null

    init {
        SpeechKit.getInstance().configure(App.context, "b6aece16-7b1e-4daa-8739-82b911d671bc")
    }

    @SuppressLint("MissingPermission")
    fun recognize() {
        recognizer?.cancel()
        recognizer = null
        recognizer = Recognizer.create(
                Recognizer.Language.RUSSIAN, Recognizer.Model.NOTES, this)
        recognizer?.start()
    }

    fun stopListening() {
        recognizer?.finishRecording()
    }

    fun vocalize(message: String) {
        vocalizer?.cancel()
        vocalizer = null
        vocalizer = Vocalizer.createVocalizer(
                Vocalizer.Language.RUSSIAN, message, true, Vocalizer.Voice.ALYSS)
        vocalizer?.setListener(this)
        vocalizer?.start()
    }

    fun stopTolkin() {
        vocalizer?.cancel()
        vocalizer = null
    }

    override fun onSynthesisBegin(vocalizer: Vocalizer) {
    }

    override fun onSynthesisDone(vocalizer: Vocalizer, synthesis: Synthesis) {
    }

    override fun onPlayingBegin(vocalizer: Vocalizer) {
        MainActivity.INSTANCE?.speechInProgress()
    }

    override fun onPlayingDone(vocalizer: Vocalizer) {
        MainActivity.INSTANCE?.speechFinished()
    }

    override fun onVocalizerError(vocalizer: Vocalizer, error: ru.yandex.speechkit.Error) {
        updateStateText(error.string)
    }

    override fun onRecordingDone(p0: Recognizer?) {
        isListening = false
        MainActivity.INSTANCE?.updateFabIcon()
    }

    override fun onSoundDataRecorded(p0: Recognizer?, p1: ByteArray?) {
    }

    override fun onPowerUpdated(p0: Recognizer?, p1: Float) {
    }

    override fun onPartialResults(p0: Recognizer?, p1: Recognition?, p2: Boolean) {
    }

    override fun onRecordingBegin(p0: Recognizer?) {
    }

    override fun onSpeechEnds(p0: Recognizer?) {
    }

    override fun onSpeechDetected(p0: Recognizer?) {
    }

    override fun onRecognitionDone(recognizer: Recognizer, recognition: Recognition) {
        val resultText = recognition.bestResultText
        SpeechProcessor.analyze(resultText)
        updateStateText(resultText)
    }

    override fun onError(p0: Recognizer?, p1: Error) {
        MainActivity.INSTANCE?.updateFabIcon()
        MainActivity.INSTANCE?.speechFinished()
    }

    private fun updateStateText(message: String) = Log.d(message)
}
