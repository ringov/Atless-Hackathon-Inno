package org.atless.hackathon.machinelearning

interface Word {

    val vocab: Array<String>
}
