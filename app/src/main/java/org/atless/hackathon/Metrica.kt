package org.atless.hackathon

import com.yandex.metrica.YandexMetrica
import org.atless.hackathon.data.Model

object Metrica {

    fun sendAddFabClick() {
        YandexMetrica.reportEvent("AddFabClick")
    }

    fun sendChannelSwitch(channelId: Long, channelTitle: String) {
        YandexMetrica.reportEvent("SwitchChannel", mapOf<String, Any>("ChannelId" to channelId, "ChannelTitle" to channelTitle))
    }

    fun sendSecretChannelAddTry(text: String) {
        YandexMetrica.reportEvent("SecretChannelAdd", mapOf<String, Any>("ChannelName" to text))
    }

    fun sendMapPinTap(eventId: Long) {
        val eventTitle = Model.events.firstOrNull { it.id == eventId }?.title ?: "Unknown"
        YandexMetrica.reportEvent("MapMarkerTap", mapOf<String, Any>("EventId" to eventId, "EventTitle" to eventTitle))
    }
}
